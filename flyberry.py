#!/usr/bin/python
from sys import argv
from datetime import datetime
import geo
import db
from isd import STATION, ISD

DEBUG = False
INBOUND_DISTANCE_IN_METERS = 50000

def debug_print(msg):
	if DEBUG:
		print msg

def query(city, begin = '2014-01-01', end = '2014-03-10'):
	#City to Geo
	loc = geo.geocode(city)
	print city + ' @ ' + str(loc)
	nearest = None
	distance = 0.0
	for station in db.get_stations():
		d = float(geo.haversine(station.latitude, station.longitude, loc[0], loc[1]))
		debug_print(str(station) + '\t in ' + str(d) + 'm')
		if nearest == None or d < distance:
			nearest = station
			distance = d
	print 'Nearest station: ' + str(nearest) + ' in ' + str(distance) + 'm'
	if distance > INBOUND_DISTANCE_IN_METERS:
		print 'There is no available station near \"'+ city + '\"'
	else:
		for isd in db.get_isd(nearest.id, begin, end):
			print str(isd)

#
# Command line example:
#   ./flyberry.py 'taipei' 2014-01-01 2014-05-01
#
if __name__ == '__main__':
	query(argv[1], argv[2], argv[3])