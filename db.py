#!/usr/bin/python
from datetime import datetime
from isd import STATION, ISD
import sqlite3
import os

dbdir = 'db'
dbfile = os.path.join(dbdir, 'noaa.db')

def initsqlite():
	if os.path.isdir(dbdir) is not True:
		os.makedirs(dbdir, 0755)
	conn = sqlite3.connect(dbfile)
	cursor = conn.cursor()
	# Create table
	cursor.execute('CREATE TABLE isd (id integer primary key, datetime timestamp, stationid text, windspeedrate integer, temperature integer, precipitation integer, isdfile text)')
	cursor.execute('CREATE TABLE station (id text primary key, latitude1000 integer, longitude1000 integer)')

	# Save (commit) the changes
	conn.commit()
	return conn


def get_conn():
	try:
		conn = sqlite3.connect(dbfile)
	except Exception, e:
		conn = initsqlite()
	return conn

def insert(isd, filename):
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute('INSERT INTO isd (datetime, stationid, windspeedrate, temperature, precipitation, isdfile) values (?, ?, ?, ?, ?, ?)', (isd.timestamp, isd.stationid, isd.windspeedrate, isd.airtemperature10, isd.get_firstprecipitation(), filename))
	conn.commit()
	cursor.execute('SELECT * FROM station WHERE id = ?', (isd.stationid,))
	if len(cursor.fetchall()) == 0:
		cursor.execute('INSERT INTO station (id, latitude1000, longitude1000) values (?, ?, ?)', (isd.stationid, isd.latitude1000, isd.longitude1000))
		conn.commit()
	conn.close()

def get_stations():
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM station")
	stations = []
	for record in cursor.fetchall():
		stations.append(STATION(record[0], record[1], record[2]))
	conn.close()
	return stations

def get_isd(stationid, begintime, endtime):
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM isd WHERE stationid = ? AND datetime(?) <= datetime(datetime) AND datetime(datetime) <= datetime(?)", (stationid, begintime, endtime))
	isdlist = []
	for record in cursor.fetchall():
		isd = ISD()
		isd.timestamp = datetime.strptime(record[1], '%Y-%m-%d %H:%M:%S')
		isd.stationid = record[2]
		isd.windspeedrate = record[3]
		isd.airtemperature10 = record[4]
		isd.set_precipitation(record[5])
		isdlist.append(isd)
	conn.close()
	return isdlist

def get_isdfile_count(isdfilename):
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute("SELECT COUNT(*) FROM isd WHERE isdfile = ?", (isdfilename,))
	count = cursor.fetchone()
	conn.close()
	return int(count[0])

def delete_from_isdfile(isdfilename):
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute("DELETE FROM isd WHERE isdfile = ?", (isdfilename,))
	conn.commit()
	conn.close()

if __name__ == '__main__':
	conn = get_conn()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM isd")
	for record in cursor.fetchall():
		print record
	cursor.execute("SELECT * FROM station")
	for record in cursor.fetchall():
		print record
	conn.close()