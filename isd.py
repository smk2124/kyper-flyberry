#!/usr/bin/python
import datetime
import re

regex_precipitation = re.compile('AAD.*AA[1-4]')

class ISD:
	def __init__(self, raw = '0000000000000001970010100000000000000000000000000000000000000000000000000000000000000000000000000000'):
		#CONTROL DATA
		self.stationid = raw[4:15]
		self.latitude1000 = int(raw[28:34])
		self.longitude1000 = int(raw[35:41])
		self.timestamp = datetime.datetime.strptime(raw[15:27], '%Y%m%d%H%M')
		#MANDATORY DATA
		self.windspeedrate = int(raw[65:69])
		self.airtemperature10 = int(raw[87:92])
		#ADDITION DATA
		self.precipitationlist = []
		for match in re.findall('AA[1-4][0-9]{8}', raw):
			self.precipitationlist.append(match)

	def __str__(self):
		buf = 'ISD:'
		## CONTROL DATA
		# Station
		#buf = buf + ' Sid:' + self.stationid
		#buf = buf + ' Geo[{0},{1}]'.format(float(self.latitude1000) / 1000, float(self.longitude1000) / 1000)
		buf = buf + ' ' + self.timestamp.strftime('%Y-%m-%dT%H:%M')
		# MANDATORY DATA
		# Wind
		buf = buf +  ' Wind:'
		if self.windspeedrate == 9999:
			buf = buf + '??m'
		else:
			buf = buf + '{0}m'.format(self.windspeedrate)
		# Temperature
		buf = buf + ' ' + str(self.airtemperature10 / 10) + 'c'
		## ADDITION DATA
		buf = buf + 'Precipitation:' + self.print_precipitation()
		return buf

	def print_precipitation(self):
		buf = ''
		for p in self.precipitationlist:
			buf = buf + ' ' + str(int(p[5:9])) +'mm/' + str(int(p[3:5])) + 'h'
		return buf

	def get_firstprecipitation(self):
		for p in self.precipitationlist:
			return int(p[5:9])
		return 0

	def set_precipitation(self, depth):
		self.precipitationlist.append('AA112%04d0' % int(depth))

class STATION:
	def __init__(self, stationid, latitude1000, longitude1000):
		self.id = stationid
		self.latitude1000 = int(latitude1000)
		self.longitude1000 = int(longitude1000)
		self.latitude = float(latitude1000) / 1000
		self.longitude = float(longitude1000) / 1000

	def __str__(self):
		return '#'+ self.id + '\t@ ' + str(self.latitude) + ',' + str(self.longitude)