#!/usr/bin/python

from math import radians, cos, sin, asin, sqrt
from sys import argv, path
path.append('./geopy')
from geopy.geocoders import GeoNames

GEONAMES_USERNAME = 'smk2124'

#gn = geocoders.GeoNames()
#print gn.geocode("Cleveland, OH 44106")

def haversine(lon1, lat1, lon2, lat2):
	"""
	Calculate the great circle distance between two points 
	on the earth (specified in decimal degrees)
	"""
	# convert decimal degrees to radians 
	lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

	# haversine formula 
	dlon = lon2 - lon1 
	dlat = lat2 - lat1 
	a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
	c = 2 * asin(sqrt(a)) 
	r = 6378137 # Radius of earth in meters. Use 3956 for miles
	return c * r

def geocode(loc):
	geolocator = GeoNames('', GEONAMES_USERNAME)
	for i in [0 , 1, 2]:
		try:
			location = geolocator.geocode(loc)
			return (location.latitude, location.longitude)
		except:
			continue
	raise 'TimeOut'

if __name__ == '__main__':
	print geocode(argv[1])

#print haversine(float(argv[1]), float(argv[2]), float(argv[3]), float(argv[4]))