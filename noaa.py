#!/usr/bin/python
from ftplib import FTP, error_perm
import datetime

import gzip
import os
import sys

import db
from isd import ISD

host = 'ftp.ncdc.noaa.gov'
root = '/pub/data/noaa'
data = 'data/noaa'

def print_progress(msg, progress = -1, total = -1):
	sandglass = ''
	if progress > 0:
		step = progress % 4
		if progress == 0:
			sandglass = '-'
		elif progress == 1:
			sandglass = '\\'
		elif progress == 2:
			sandglass = '|'
		elif progress == 3:
			sandglass = '/'
	bar = '' 
	if total > 0:
		bar = '|'
		persentage = progress * 20 / total
		for i in range(0, 20):
			if i <= persentage:
				bar = bar + '='
			elif i == persentage + 1 and len(sandglass) > 0:
				bar = bar + sandglass
			else:
				bar = bar + ' '
		bar = bar + '|' + str(persentage * 5) + '%' 

	if len(bar) > 0:
		msg = '\r' + msg + ' ' + bar
	elif len(sandglass) > 0:
		msg = '\r' + msg + ' ' + sandglass
	else:
		msg = '\r' + msg
	sys.stdout.write('\033[2K\r' + msg)
	sys.stdout.flush()

def ftp_login(path):
	for i in range(3):
		try:
			ftp = FTP(host)
			ftp.login()
			ftp.cwd(path)
			return ftp
		except error_perm as e:
			print e.message
			continue
	return None


def ftp_list(ftp):
	#files = ftp.dir()
	#files = ftp.retrlines('LIST')
	return ftp.nlst()

def download(year):
	path = os.path.join(root, str(year))
	local = os.path.join(data, str(year))
	ftp = ftp_login(path)
	if ftp is None:
		sys.exit('Cannot connect FTP of NOAA')
	files = ftp_list(ftp)
	if os.path.isdir(local) is not True:
		os.makedirs(local, 0755)
	count = 1
	total = len(files)
	for f in files:
		savepath = os.path.join(local, f)
		sys.stdout.write('\rDownloading(' + str(count) + '/' + str(total) + '): ' + f + ' to ' + savepath)
		sys.stdout.flush()
		count = count + 1

		if os.path.isfile(savepath) is not True:
			with open(savepath, 'w') as localfile:
				ftp.retrbinary('RETR %s' % f, localfile.write)
	print

	ftp.quit()

def sync(year):
	#printcount = 0
	path = os.path.join(root, str(year))
	localdir = os.path.join(data, str(year))
	if os.path.isdir(localdir):
		files = os.listdir(localdir)
		filecount = 0
		filetotal = len(files)
		for f in files:
			filecount = filecount + 1
			if f.endswith(".gz") is not True:
				continue
			isdfilepath = os.path.join(localdir, f)
			isdfilelinecount = 0
			print_progress('Importing ' + f + '(' + str(filecount) + '/' + str(filetotal) + ') ')
			try:
				with gzip.open(isdfilepath, 'rb') as gzfile:
					isdfilelinecount = sum(1 for _ in gzfile)
			except:
				os.rename(isdfilepath, isdfilepath + '.bad')
				print f + ' is broken'
				continue
			if isdfilelinecount != db.get_isdfile_count(f):
				db.delete_from_isdfile(f)
				isdcount = 0
				with gzip.open(isdfilepath, 'rb') as gzfile:
					for line in gzfile:
						print_progress('Importing ' + f + '(' + str(filecount) + '/' + str(filetotal) + ') ', isdcount, isdfilelinecount)
						isdcount = isdcount + 1
						#if printcount > 100 :
						#	return
						isd = ISD(line)
						db.insert(isd, f)
						#printcount = printcount + 1

if __name__ == '__main__':
	for i in range(1, len(sys.argv)):
		print 'Downloading year ' + sys.argv[i]
		download(sys.argv[i])
		print 'Importing year ' + sys.argv[i]
		sync(sys.argv[i])
